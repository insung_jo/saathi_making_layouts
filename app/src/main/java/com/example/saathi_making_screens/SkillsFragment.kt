package com.example.saathi_making_screens

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment

class SkillsFragment : Fragment() {

    val selectedChapterArray: Array<Boolean> = arrayOf(false, false, false, false, false, false, false)

    var isSelectedAll: Boolean = false

    lateinit var ll_chapter_1: LinearLayout
    lateinit var ll_chapter_2: LinearLayout
    lateinit var ll_chapter_3: LinearLayout
    lateinit var ll_chapter_4: LinearLayout
    lateinit var ll_chapter_5: LinearLayout
    lateinit var ll_chapter_6: LinearLayout
    lateinit var ll_chapter_7: LinearLayout


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        loadCurrentlySelectedChapters()


        return inflater.inflate(R.layout.fragment_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ll_chapter_1 = view.findViewById(R.id.ll_skills_ch_1)
        ll_chapter_2 = view.findViewById(R.id.ll_skills_ch_2)
        ll_chapter_3 = view.findViewById(R.id.ll_skills_ch_3)
        ll_chapter_4 = view.findViewById(R.id.ll_skills_ch_4)
        ll_chapter_5 = view.findViewById(R.id.ll_skills_ch_5)
        ll_chapter_6 = view.findViewById(R.id.ll_skills_ch_6)
        ll_chapter_7 = view.findViewById(R.id.ll_skills_ch_7)

        if (isSelectedAll){
            ll_chapter_1.visibility = View.VISIBLE
            ll_chapter_2.visibility = View.VISIBLE
            ll_chapter_3.visibility = View.VISIBLE
            ll_chapter_4.visibility = View.VISIBLE
            ll_chapter_5.visibility = View.VISIBLE
            ll_chapter_6.visibility = View.VISIBLE
            ll_chapter_7.visibility = View.VISIBLE

        }else {
            if (selectedChapterArray[0]) {
                ll_chapter_1.visibility = View.VISIBLE
            }
            if (selectedChapterArray[1]) {
                ll_chapter_2.visibility = View.VISIBLE
            }
            if (selectedChapterArray[2]) {
                ll_chapter_3.visibility = View.VISIBLE
            }
            if (selectedChapterArray[3]) {
                ll_chapter_4.visibility = View.VISIBLE
            }
            if (selectedChapterArray[4]) {
                ll_chapter_5.visibility = View.VISIBLE
            }
            if (selectedChapterArray[5]) {
                ll_chapter_6.visibility = View.VISIBLE
            }
            if (selectedChapterArray[6]) {
                ll_chapter_7.visibility = View.VISIBLE
            }
        }

    }

    fun loadCurrentlySelectedChapters() {

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return

        isSelectedAll = sharedPref.getBoolean("isSelectedAll", false)

        selectedChapterArray[0] = sharedPref.getBoolean("currentlySelectedChaptersArray_0", false)
        selectedChapterArray[1] = sharedPref.getBoolean("currentlySelectedChaptersArray_1", false)
        selectedChapterArray[2] = sharedPref.getBoolean("currentlySelectedChaptersArray_2", false)
        selectedChapterArray[3] = sharedPref.getBoolean("currentlySelectedChaptersArray_3", false)
        selectedChapterArray[4] = sharedPref.getBoolean("currentlySelectedChaptersArray_4", false)
        selectedChapterArray[5] = sharedPref.getBoolean("currentlySelectedChaptersArray_5", false)
        selectedChapterArray[6] = sharedPref.getBoolean("currentlySelectedChaptersArray_6", false)
    }
}