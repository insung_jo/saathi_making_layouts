package com.example.saathi_making_screens

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment

class ChaptersFragment : Fragment() {

    lateinit var ll_all_chapters: LinearLayout
    lateinit var ll_chapter_1: LinearLayout
    lateinit var ll_chapter_2: LinearLayout
    lateinit var ll_chapter_3: LinearLayout
    lateinit var ll_chapter_4: LinearLayout
    lateinit var ll_chapter_5: LinearLayout
    lateinit var ll_chapter_6: LinearLayout
    lateinit var ll_chapter_7: LinearLayout

    var ch_all: Boolean = false
    val selectedChapterArray: Array<Boolean> = arrayOf(false, false, false, false, false, false, false)

    var next: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_chapters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ll_all_chapters = view.findViewById(R.id.ll_All_Chapters)

        ll_chapter_1 = view.findViewById(R.id.ll_Chapter_1)
        ll_chapter_2 = view.findViewById(R.id.ll_Chapter_2)
        ll_chapter_3 = view.findViewById(R.id.ll_Chapter_3)
        ll_chapter_4 = view.findViewById(R.id.ll_Chapter_4)
        ll_chapter_5 = view.findViewById(R.id.ll_Chapter_5)
        ll_chapter_6 = view.findViewById(R.id.ll_Chapter_6)
        ll_chapter_7 = view.findViewById(R.id.ll_Chapter_7)

        initClickListeners()
    }

    fun checkNext() {
        if (ch_all || selectedChapterArray.contains(true)){
            next = true
            (activity as MainActivity).authorizeChaptersNext()
        } else{
            next = false
            (activity as MainActivity).unAuthorizeChaptersNext()
        }

//        if (ch_all){
//            for (i in selectedChapterArray.indices){
//                selectedChapterArray[i] = true
//                println("$i 번째 값: ${selectedChapterArray[i]}")
//            }
//        }

        saveCurrentlySelectedChapters()
    }

    fun saveCurrentlySelectedChapters(){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {

            putBoolean("isSelectedAll", ch_all)

            putBoolean("currentlySelectedChaptersArray_0", selectedChapterArray[0])
            putBoolean("currentlySelectedChaptersArray_1", selectedChapterArray[1])
            putBoolean("currentlySelectedChaptersArray_2", selectedChapterArray[2])
            putBoolean("currentlySelectedChaptersArray_3", selectedChapterArray[3])
            putBoolean("currentlySelectedChaptersArray_4", selectedChapterArray[4])
            putBoolean("currentlySelectedChaptersArray_5", selectedChapterArray[5])
            putBoolean("currentlySelectedChaptersArray_6", selectedChapterArray[6])

            commit()
        }
    }

    fun initClickListeners() {
        ll_all_chapters.setOnClickListener{
            if (!ch_all) {
                makeAllChaptersSelected()
            }else{
                makeAllChaptersCanceled()
            }
            checkNext()
        }

        /**
         * 1~7번까지 클릭 리스너 달기
         * */
        ll_chapter_1.setOnClickListener{
            if (!selectedChapterArray[0]) {
                ll_chapter_1.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[0] = true
            }else{
                ll_chapter_1.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[0] = false
            }
            checkNext()
        }
        ll_chapter_2.setOnClickListener{
            if (!selectedChapterArray[1]) {
                ll_chapter_2.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[1] = true
            }else{
                ll_chapter_2.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[1] = false
            }
            checkNext()
        }
        ll_chapter_3.setOnClickListener{
            if (!selectedChapterArray[2]) {
                ll_chapter_3.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[2] = true
            }else{
                ll_chapter_3.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[2] = false
            }
            checkNext()
        }
        ll_chapter_4.setOnClickListener{
            if (!selectedChapterArray[3]) {
                ll_chapter_4.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[3] = true
            }else{
                ll_chapter_4.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[3] = false
            }
            checkNext()
        }
        ll_chapter_5.setOnClickListener{
            if (!selectedChapterArray[4]) {
                ll_chapter_5.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[4] = true
            }else{
                ll_chapter_5.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[4] = false
            }
            checkNext()
        }
        ll_chapter_6.setOnClickListener{
            if (!selectedChapterArray[5]) {
                ll_chapter_6.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[5] = true
            }else{
                ll_chapter_6.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[5] = false
            }
            checkNext()
        }
        ll_chapter_7.setOnClickListener{
            if (!selectedChapterArray[6]) {
                ll_chapter_7.setBackgroundResource(R.drawable.dark_sage_btn)
                makeAllChaptersCanceled()
                selectedChapterArray[6] = true
            }else{
                ll_chapter_7.setBackgroundResource(R.drawable.white_seven_btn)
                selectedChapterArray[6] = false
            }
            checkNext()
        }
    }



    // All Chapters 레이아웃이 선택되는 경우
    fun makeAllChaptersSelected() {
        ll_all_chapters.setBackgroundResource(R.drawable.dark_sage_btn)

        ll_chapter_1.setBackgroundResource(R.drawable.white_seven_btn)
        ll_chapter_2.setBackgroundResource(R.drawable.white_seven_btn)
        ll_chapter_3.setBackgroundResource(R.drawable.white_seven_btn)
        ll_chapter_4.setBackgroundResource(R.drawable.white_seven_btn)
        ll_chapter_5.setBackgroundResource(R.drawable.white_seven_btn)
        ll_chapter_6.setBackgroundResource(R.drawable.white_seven_btn)
        ll_chapter_7.setBackgroundResource(R.drawable.white_seven_btn)

        ch_all = true
    }

    // All Chapters 레이아웃이 취소되는 경우
    fun makeAllChaptersCanceled() {
        ll_all_chapters.setBackgroundResource(R.drawable.white_seven_btn)
        ch_all = false
    }

}