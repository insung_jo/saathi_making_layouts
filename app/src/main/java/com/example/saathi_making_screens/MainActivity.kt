package com.example.saathi_making_screens

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    // 상단 버튼 중 Chapters 버튼 구성요소
    private lateinit var llChapters: LinearLayout
    private lateinit var tvChapters: TextView
    private lateinit var ivChaptersReset: ImageView
    private var chaptersResettable: Boolean = false
    private var chapters_next: Boolean = false


    // 상단 버튼 중 Skills 버튼 구성요소
    private lateinit var llSkills: LinearLayout
    private lateinit var tvSkills: TextView
    private lateinit var ivSkillsReset: ImageView
    private var skillsResettable: Boolean = false
    private var skills_next: Boolean = false


    // 상단 버튼 중 Difficulty 버튼 구성요소
    private lateinit var llDifficulty: LinearLayout
    private lateinit var tvDifficulty: TextView
    private lateinit var ivDifficultyReset: ImageView
    private var difficultyResettable: Boolean = false
    private var difficulty_next: Boolean = false


    // 최하단 next 버튼
    private lateinit var ll_next: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindViews()

        setClickListeners()

        showFirstFragment()
    }

    fun bindViews() {
        // 상단 버튼 중 Chapters 버튼 구성요소 관련
        llChapters = findViewById(R.id.ll_chapters)
        tvChapters = findViewById(R.id.tv_chapters)
        ivChaptersReset = findViewById(R.id.iv_chapter_complete)

        // 상단 버튼 중 Skills 버튼 구성요소 관련
        llSkills = findViewById(R.id.ll_skills)
        tvSkills = findViewById(R.id.tv_skills)
        ivSkillsReset = findViewById(R.id.iv_skills_complete)

        // 상단 버튼 중 Difficulty 버튼 구성요소 관련
        llDifficulty = findViewById(R.id.ll_difficulty)
        tvDifficulty = findViewById(R.id.tv_difficulty)
        ivDifficultyReset = findViewById(R.id.iv_difficulty_complete)

        // 최하단 next 버튼
        ll_next = findViewById(R.id.ll_next)
    }

    fun setClickListeners() {

        // Chapters 버튼
        llChapters.setOnClickListener {
            if (chaptersResettable) {
                resetChapters()
            }
        }

        // 최하단 next 버튼
        ll_next.setOnClickListener {
            if (difficulty_next) {
                println("The end")
            } else if (skills_next) {
                println("go to difficulty")
            } else if (chapters_next) {
                goToSkills()
            }
        }
    }

    fun showFirstFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container, ChaptersFragment())
            .commit()
    }

    fun goToSkills() {

        // 일단 프래그먼트 갈아 끼워주기
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container, SkillsFragment())
            .commit()

        /**
         * 액티비티 부분 변경(상단 버튼, 하단 Next 버튼)
         * */
        unAuthorizeNextButton() // 하단 next 버튼 색상 변경

        // 상단 Chapter 버튼 완성된 모양으로 변경
        llChapters.setBackgroundResource(R.drawable.white_btn_with_pine_border)
        ivChaptersReset.setImageResource(R.drawable.icon_reset_gr)
        tvChapters.setTextColor(resources.getColor(R.color.color_pine))

        // 상단 Skills 버튼 진행중인 모양으로 변경
        llSkills.setBackgroundResource(R.drawable.pine_btn)
        tvSkills.setTextColor(resources.getColor(R.color.color_white))

    }

    fun resetChapters() {

        // 안쪽 프래그먼트는 아예 새로 갈아끼우기
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container, ChaptersFragment())
            .commit()

        /**
         * 액티비티 부분은 직접 되돌려주기
         * */
        // 1. 버튼 3개 모두 초기화시키고
        initChaptersButton()
        initSkillsButton()
        initDifficultyButton()

        // 2. 최하단 next 버튼도 비활성화시켜준다.
        unAuthorizeChaptersNext()
    }

    // Chapters 버튼 처음 상태로 되돌리기
    fun initChaptersButton() {
        llChapters.setBackgroundResource(R.drawable.pine_btn)
        tvChapters.setTextColor(resources.getColor(R.color.color_white))
        ivChaptersReset.setImageResource(R.drawable.icon_reset_wt)
        ivChaptersReset.visibility = View.GONE
        chaptersResettable = false
    }

    // Skills 버튼 처음 상태로 되돌리기
    fun initSkillsButton() {
        llSkills.setBackgroundResource(R.drawable.white_btn_with_grey_border)
        tvSkills.setTextColor(resources.getColor(R.color.color_very_light_pink_four_2))
        ivSkillsReset.setImageResource(R.drawable.icon_reset_wt)
        ivSkillsReset.visibility = View.GONE
        skillsResettable = false
    }

    // Difficulty 버튼 처음 상태로 되돌리기
    fun initDifficultyButton() {
        llDifficulty.setBackgroundResource(R.drawable.white_btn_with_grey_border)
        tvDifficulty.setTextColor(resources.getColor(R.color.color_very_light_pink_four_2))
        ivDifficultyReset.setImageResource(R.drawable.icon_reset_wt)
        ivDifficultyReset.visibility = View.GONE
        difficultyResettable = false
    }


    /**
     * Chapters에서 다음단계로 이동 허가/불허가
     * */
    fun authorizeChaptersNext() {
        authorizeNextButton()
        chapters_next = true
        ivChaptersReset.visibility = View.VISIBLE
        chaptersResettable = true
    }

    fun unAuthorizeChaptersNext() {
        unAuthorizeNextButton()
        chapters_next = false
        ivChaptersReset.visibility = View.GONE
        chaptersResettable = false
    }


    /**
     * 최하단 next 버튼 활성화/비활성화(색깔만 바꿔주는 부분)
     * */
    fun authorizeNextButton() {
        ll_next.setBackgroundResource(R.color.color_pine)
    }

    fun unAuthorizeNextButton() {
        ll_next.setBackgroundResource(R.color.greyish_teal)
    }

}